//https://jobs.github.com/positions.json?description=react

const formulario = document.querySelector('#formulario');
const resultado = document.querySelector('#resultado');

document.addEventListener('DOMContentLoaded', main);

function main() {
  formulario.addEventListener('submit', validarFormulario);
}

function validarFormulario(e) {
  e.preventDefault();

  const busqueda = document.querySelector('#busqueda').value.trim();

  if (busqueda.length <= 3) {
    alerta('Palabra clave muy corta');
    return;
  }

  consultarAPI(busqueda);


}

function alerta(texto) {

  const previa = document.querySelector('.alerta');
  if (previa) {
    return;
  }

  const alerta = document.createElement('div');
  alerta.classList.add('bg-gray-100', 'p-3', 'text-center', 'mt-3', 'alerta');
  alerta.textContent = texto;

  formulario.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 3000);
}

function consultarAPI(busqueda) {
  const URI = `https://jobs.github.com/positions.json?description=${busqueda}`;

  const cors = `https://api.allorigins.win/get?url=${encodeURIComponent(URI)}`;

  axios.get(cors)
    .then( response => {
      if (parseInt(response.status) === 200) {
        mostrarVacantes(JSON.parse(response.data.contents));
      }
    })
    .catch( error => {
      alerta('Problema al comunicar con API: '+error)
    })
}

function mostrarVacantes(vacantes) {
  limpiarVacantes();
  if (vacantes.length > 0) {
    resultado.classList.add('grid');
    vacantes.forEach(vacante => {
      const { company, title, type, url } = vacante;
      resultado.innerHTML += `
        <div class="shadow bg-white p-6 rounded">
          <h2 class="text-2xl font-light mb-4">${title}</h2>
          <p class="font-bold uppercase">Compañia:  <span class="font-light normal-case">${company} </span></p>
          <p class="font-bold uppercase">Tipo de Contrato:   <span class="font-light normal-case">${type} </span></p>
          <a class="bg-teal-500 max-w-lg mx-auto mt-3 rounded p-2 block uppercase font-xl font-bold text-white text-center" href="${url}">Ver Vacante</a>
        </div>
      `;
    });
  } else {
    resultado.classList.remove('grid');
    const sin = document.createElement('p');
    sin.classList.add('text-center', 'mt-5', 'text-gray-600', 'w-full');
    sin.textContent = 'No hay resultados con tus terminos de busqueda';
    resultado.appendChild(sin);
  }
}

function limpiarVacantes() {
  while (resultado.firstChild) {
    resultado.removeChild(resultado.firstChild);
  }
}